﻿# Bachelor thesis project
Project with a simple django Blog app for bachelor thesis - Petr Kučera 2020
It is going to be used for analyzing 6 different localization methodics.
*Master* branch contains only the basic app without any localization solution implemented (except the default Django solution).

To start up the project you have to:
1. Download the repo
2. Go to root of the project
3. Activate environment `source /env/bin/activate`
4. Prepare project database
```
python3 manage.py migrate
python3 manage.py loaddata dump.json
```
5. Start the app
```
python3 manage.py runserver
```
6. Open http://127.0.0.1:8000/blog
__*Other solution specific steps are described in the thesis__

(Optional) There is `dump.json` file with testing data.
To load the data, run
```
./manage.py loaddata dump.json 
```

## Analyzed solutions
**Each of the implemented localization methods has it's own branch**
1. Django basic - implemented in branch *add-basic-translations*
2. Django translation manager (DTM) - implemented in branch *add-django-translation-manager-localization*
3. Rosetta - implemented in branch *add-rosetta-localization*
4. Lokalise - implemented in branch *add-lokalise-localization*
5. Zanata - implemented in branch *add-zanata-localization*
6. Weblate - implemented in branch *add-weblate-localization*