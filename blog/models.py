from django.db import models
from django.utils import timezone
from django.utils.translation import pgettext_lazy, gettext_lazy as _


class BasicDateMixin(models.Model):
    class Meta:
        abstract = True
        ordering = ("-created",)

    created = models.DateTimeField(_('date of creation'), default=timezone.now, editable=False)
    updated = models.DateTimeField(_('date of modification'), null=True, blank=True, editable=False)


class LikeMixin(models.Model):
    class Meta:
        abstract = True

    likes = models.IntegerField(_("Like count"), default=0, editable=False)


class Post(BasicDateMixin, LikeMixin):
    title = models.CharField(pgettext_lazy('post title', 'title'), max_length=90)
    author = models.CharField(_('author'), max_length=120)
    text = models.TextField(_('text'))

    views = models.IntegerField(verbose_name="View count", default=0, editable=False)

    def __str__(self):
        return 'Post created {} by {}'.format(self.created, self.author)


class Comment(BasicDateMixin, LikeMixin):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(_('author'), max_length=120)
    text = models.TextField(_('text'))

    def __str__(self):
        return 'Comment {} by {}'.format(self.text, self.author)
