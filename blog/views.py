from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views import generic

from blog.models import Post, Comment
from blog.forms import CommentForm


def index(request):
    return render(request=request, template_name='blog/index.html')


class PostsView(generic.ListView):
    template_name = 'blog/posts.html'
    context_object_name = 'latest_post_list'

    def get_queryset(self):
        return Post.objects.all()[:10]


def post_detail(request, post_id):
    template_name = 'blog/postDetail.html'
    post = get_object_or_404(Post, id=post_id)
    comments = post.comments.all()
    new_comment = None
    # Comment posted
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():

            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            # Assign the current post to the comment
            new_comment.post = post
            # Save the comment to the database
            new_comment.save()
    else:
        comment_form = CommentForm()

    return render(request, template_name, {'post': post,
                                           'comments': comments,
                                           'new_comment': new_comment,
                                           'comment_form': comment_form})

